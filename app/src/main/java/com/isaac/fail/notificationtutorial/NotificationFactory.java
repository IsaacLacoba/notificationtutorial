package com.isaac.fail.notificationtutorial;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

/**
 * Created by isaac on 11/01/16.
 */
public class NotificationFactory {
    public static void create_notification(Context context, Class activity_class) {
        Log.i("[NotificationFactory]", "create_notification()*********");

        // prepare intent which is triggered if the
        // notification is selected
        Intent intent = new Intent(context, activity_class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        // use System.currentTimeMillis() to have a unique ID for the pending intent
        int UID = (int) System.currentTimeMillis();
        int flags = PendingIntent.FLAG_UPDATE_CURRENT;
        Log.i("[NotificationFactory]", "UID: "+ UID +"*********");
        PendingIntent pIntent = PendingIntent.getActivity(context, UID, intent, flags);

        // build notification
        // the addAction re-use the same intent to keep the example short
        Notification notification = new Notification.Builder(context)
                .setContentTitle("New mail from " + "test@gmail.com")
                .setContentText("Subject")
                .setSmallIcon(R.drawable.icon)
                .setContentIntent(pIntent)
                .setAutoCancel(true)
                .addAction(R.drawable.icon, "And more", pIntent).build();


        NotificationManager notificationManager = (NotificationManager)
                context.getSystemService(context.NOTIFICATION_SERVICE);
        // hide the notification after its selected
        notification.flags |= Notification.FLAG_AUTO_CANCEL;
        int ID = 0;
        notificationManager.notify(ID, notification);
    }
}
