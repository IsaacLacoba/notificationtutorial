/*
 * Copyright (c) 2016 The BQ Project . Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

package com.isaac.fail.notificationtutorial;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;

/**
 * Created by isaac on 11/01/16.
 */
public class NotificationReceiver extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.i("[NotificationReceiver]", "onCreate*********");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.result);
        NotificationFactory.create_notification(getBaseContext(), MainActivity.class);
    }

}
